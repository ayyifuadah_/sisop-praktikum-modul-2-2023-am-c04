filter (12).c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

#define ZIP_FILENAME "players.zip"

void delete_png_files(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                if (strstr(entry->d_name, "ManUtd") == NULL) {
                    sprintf(file_path, "%s/%s", dir_name, entry->d_name);
                    if (remove(file_path) == 0) {
                        printf("File %s deleted successfully\n", entry->d_name);
                    } else {
                        printf("Error: Unable to delete file %s\n", entry->d_name);
                    }
                }
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}

void categorize_players(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000], cmd[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                char *file_name = entry->d_name;
                char player_name[100], player_team[100], player_position[100];
                int player_rating;

                // Parse player name, team, position, and rating from file name
                sscanf(file_name, "%[^_]_%[^_]_%[^_]_%d.png", player_name, player_team, player_position, &player_rating);

                // Build directory path based on player position
                char dir_path[1000];
                if (strcmp(player_position, "Kiper") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Kiper");
                } else if (strcmp(player_position, "Bek") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Bek");
                } else if (strcmp(player_position, "Gelandang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Gelandang");
                } else if (strcmp(player_position, "Penyerang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Penyerang");
                } else {
                    printf("Error: Unknown player position %s\n", player_position);
                    continue;
                }

                // Create directory for player position if it doesn't exist
                struct stat st = {0};
                if (stat(dir_path, &st) == -1) {
                    sprintf(cmd, "mkdir %s", dir_path);
                    if (system(cmd) == -1) {
                        printf("Error: Unable to create directory %s\n", dir_path);
                        continue;
                    }
                }

                // Move file to appropriate directory
                sprintf(file_path, "%s/%s", dir_name, entry->d_name);
                sprintf(cmd, "mv %s %s", file_path, dir_path);
                if (system(cmd) == -1) {
                    printf("Error: Unable to move file %s to directory %s\n", entry->d_name, dir_path);
                    continue;
                } else {
                    printf("File %s moved to directory %s successfully\n", entry->d_name, dir_path);
                }
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}
void best_player(char *dir_name, char *position, int num_players) {
    char output_file[] = "Best_Player.txt";
    DIR *dir;
    struct dirent *entry;
    char file_path[1000], cmd[1000];

    int ratings[num_players];
    char *players[num_players];

    int i = 0;
    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL && i < num_players) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                char *file_name = entry->d_name;
                char player_name[100], player_team[100], player_position[100];
                int player_rating;

                // Parse player name, team, position, and rating from file name
                sscanf(file_name, "%[^_]_%[^_]_%[^_]_%d.png", player_name, player_team, player_position, &player_rating);

                // Check if player position matches function argument
                if (strcmp(player_position, position) == 0) {
                    // Add player to array of potential players
                    ratings[i] = player_rating;
                    players[i] = malloc(strlen(player_name) + 1);
                    strcpy(players[i], player_name);
                    i++;
                }
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
        return;
    }

    // Find the highest rated players
    for (int j = 0; j < i - 1; j++) {
        int max_rating = ratings[j];
        int max_index = j;
        for (int k = j + 1; k < i; k++) {
            if (ratings[k] > max_rating) {
                max_rating = ratings[k];
                max_index = k;
            }
        }

        // Swap the highest rated player to the front of the array
        int temp_rating = ratings[j];
        ratings[j] = ratings[max_index];
        ratings[max_index] = temp_rating;

        char *temp_player = players[j];
        players[j] = players[max_index];
        players[max_index] = temp_player;
    }

    // Write the best players to a file
    FILE *fp = fopen(output_file, "w");
    if (fp == NULL) {
        printf("Error: Unable to open file %s for writing\n", output_file);
        return;
    }
    fprintf(fp, "%s:\n", position);
    for (int j = 0; j < num_players; j++) {
        fprintf(fp, "- %s (%d)\n", players[j], ratings[j]);
    }
    fclose(fp);

    // Free allocated memory
    for (int j = 0; j < i; j++) {
        free(players[j]);
    }
}



int main() {
    char *url = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char *zip_dir = ".";
    char zip_file[1000];
    sprintf(zip_file, "%s/%s", zip_dir, ZIP_FILENAME);

    pid_t pid = fork();
    if (pid == -1) {
        printf("Error forking process\n");
        return -1;
    } else if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-O", zip_file, url, NULL);
        exit(127);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            pid = fork();
            if (pid == -1) {
                printf("Error forking process\n");
                return -1;
            } else if (pid == 0) {
                execl("/usr/bin/unzip", "unzip", "-q", zip_file, "-d", zip_dir, NULL);
                exit(127);
            } else {
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    // Filter the files and delete all files in the "players" directory except for "ManUtd.txt"
                    // Filter the files and delete all files in the "players" directory except for "ManUtd.txt"
                    delete_png_files("players");
                    categorize_players("players");
                    best_player("players/Bek", "Bek", 4);
                    best_player("players/Gelandang", "Gelandang", 4);
                    best_player("players/Penyerang", "Penyerang", 2);
                    best_player("players/Kiper", "Kiper", 1);



                    if (remove(zip_file) != 0) {
                    printf("Error deleting zip file\n");
                    return -1;
            } else {
    printf("ZIP file deleted successfully\n");
}
            } else {
            printf("Error extracting zip file\n");
            return -1; 
        }
    }
    } else {
    printf("Error downloading file\n");
    return -1;
    }
    }
    return 0;
}