#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>

#define MAX_LEN 256

int main(int argc, char *argv[]) {
    
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <fileUnzip>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *a = strrchr(argv[1], '.');
    if (a == NULL || strcmp(a, ".zip") != 0) {
        fprintf(stderr, "Failed\n");
        exit(EXIT_FAILURE);
    }
    
    char cmd[MAX_LEN];
    snprintf(cmd, MAX_LEN, "unzip %s", argv[1]);
    
    int status;
    pid_t child_id;
    child_id = fork();
    if (child_id == -1) {
        fprintf(stderr, "Failed(): %s\n", strerror(errno));
        exit(EXIT_FAILURE);
        
    } else if (child_id == 0) {
        system(cmd);
        exit(EXIT_SUCCESS);
    } 

    return 0;
}

